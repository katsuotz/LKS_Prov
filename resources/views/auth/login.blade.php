<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="{{ url('css/style.css') }}">
</head>
<body>
<div class="form-wrapper">
<form action="{{ url('login') }}" method="post">
	{{ csrf_field() }}
	<div>
		<input type="text" name="username" placeholder="Username">
	</div>
	<div>
		<input type="password" name="password" placeholder="Password">
	</div>
	<div>
		<input type="submit" value="Login" class="button">
	</div>
</form>
</div>
</body>
</html>