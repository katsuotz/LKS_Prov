@extends('layouts.app')
@section('content')
<div class="exam-data">
<div>{{ $data->nama_soal }} kelas {{ $data->kelas->nama_kelas }}</div>
<div>{{ date("d F Y H:i:s", strtotime($data->start)) }} - {{ date("d F Y H:i:s", strtotime($data->end)) }}</div>
</div>

<div class="table-wrapper">
	<h3>Student List</h3>
	<table border="1">
		<tr>
			<th>Student ID</th>
			<th>Student Name</th>
			<th>Score</th>
			<th>Action</th>
		</tr>
		@foreach (\App\Jawab::where('id_soal', $data->id_soal)->get() as $value)
		<tr>
			<td>{{ $value->user->siswa->nis }}</td>
			<td>{{ $value->user->siswa->nama_siswa }}</td>
			<td>{{ $value->nilai ? $value->nilai : 'Belum Dinilai' }}</td>
			<td>
				<a href="{{ url('assess_question/' . $value->id_jawab) }}" class="button">Assess</a>
			</td>
		</tr>
		@endforeach
	</table>
</div>

@endsection