@extends('layouts.app')
@section('content')
<div class="form-wrapper">
	<form action="{{ url('store_exam') }}" method="POST">
		{{ csrf_field() }}
		<div>
			<input type="text" name="nama_soal" placeholder="Exam Title">
		</div>

		<div>
			<select name="id_kelas">
				<!-- get options from database -->
				<option selected>--Select Classroom--</option>
				@foreach (\App\Kelas::all() as $val)
				<option value="{{ $val->id_kelas }}">{{ $val->nama_kelas }}</option>
				@endforeach
			</select>
		</div>
		<div>
			<div class="input-title">Start Exam Time</div>
			<input type="date" name="start_date">
			<input type="time" name="start_time">
		</div>
		<div>
			<div class="input-title">End Exam Time</div>
			<input type="date" name="end_date">
			<input type="time" name="end_time">
		</div>
		<div>
			<input type="submit" value="Create New Exam" class="button">
		</div>
	</form>
</div>
<div class="table-wrapper">
	<h3>Manage Exam Question</h3>
	<table border="1">
		<tr>
			<th>Title</th>
			<th>Classroom</th>
			<th>Start Time</th>
			<th>End Time</th>
			<th>Action</th>
		</tr>
		@foreach (\App\Soal::all() as $value)
		<tr>
			<td>{{ $value->nama_soal }}</td>
			<td>{{ $value->kelas->nama_kelas }}</td>
			<td>{{ date("d F Y H:i:s", strtotime($value->start)) }}</td>
			<td>{{ date("d F Y H:i:s", strtotime($value->end)) }}</td>
			<td>
				<a href="{{ url('manage_exam/' . $value->id_soal) }}" class="button">Manage Question</a>
				<a href="{{ url('delete_exam/' . $value->id_soal) }}" class="button">Delete</a>
			</td>
		</tr>
		@endforeach
	</table>
</div>
@endsection