<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class GuruController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
		$this->middleware('guru');
	}

	public function index()
	{
		return view('guru.home');
	}

	public function create_exam()
	{
		return view('guru.create_exam');
	}

	public function store_exam(Request $request)
	{
		$this->validate($request, [
			'nama_soal' => 'required',
			'id_kelas' => 'required|exists:kelas',
			'start_date' => 'required',
			'start_time' => 'required',
			'end_date' => 'required',
			'end_time' => 'required',
		]);

		$input = $request->all();

		$data = [
			'nama_soal' => $input['nama_soal'],
			'id_kelas' => $input['id_kelas'],
			'start' => $input['start_date'] . ' ' . $input['start_time'],
			'end' => $input['end_date'] . ' ' . $input['end_time'],
		];

		if ($data['end'] > $data['start']) {
			$soal = \App\Soal::create($data);
			return redirect('/manage_exam/' . $soal->id_soal);
		}

		return redirect('/create_exam');
	}

	public function manage_exam($id='')
	{
		return view('guru.manage_exam', [
			'data' => \App\Soal::where('id_soal', $id)->first()
		]);
	}

	public function delete_exam($id='')
	{
		$soal = \App\Soal::where('id_soal', $id)->delete();
		return redirect('/create_exam');
	}

	public function store_question($id='', $tipe='', Request $request)
	{
		if ($tipe == 'pg') {
			$this->validate($request, [
				'question' => 'required',
				'optionA' => 'required',
				'optionB' => 'required',
				'optionC' => 'required',
				'optionD' => 'required',
				'optionE' => 'required',
				'weight' => 'required',
				'kj' => 'required',
			]);
		} elseif ($tipe == 'essay') {
			$this->validate($request, [
				'question' => 'required',
				'weight' => 'required',
				'kj' => 'required',
			]);
		}

		$input = $request->all();

		$input['tipe'] = $tipe;
		$input['id_soal'] = $id;
		$soal = \App\Soal::find($id)->first();
		$weight = 0;
		foreach ($soal->detail_soal as $key => $value) {
			$weight+=$value->weight;
		}

		if ($input['weight'] <= 100 - $weight) {
			\App\DetailSoal::create($input);
		}

		return redirect('/manage_exam/' . $id);
	}

	public function delete_question($id='')
	{
		$get = \App\DetailSoal::where('id_detail_soal', $id);
		$soal = $get->first();
		$get->delete();
		return redirect('/manage_exam/' . $soal->id_soal);
	}

	public function assess_exam()
	{
		return view('guru.assess_exam');
	}

	public function assess_exam_id($id='')
	{
		return view('guru.assess_exam_id', [
			'data' => \App\Soal::where('id_soal', $id)->first()
		]);
	}

	public function assess_question($id='')
	{
		return view('guru.assess_question', [
			'data' => \App\Jawab::where('id_jawab', $id)->first()
		]);
	}

	public function store_score($id='', Request $request)
	{
		$this->validate($request, [
			'skor.*' => 'required|min:0|max:100'
		]);
		$input = $request->all();
		$get = \App\Jawab::where('id_jawab', $id);
		$data = $get->first();
		$totalskor = 0;
		foreach ($data->detail_jawab as $key => $value) {
			if ($value->detail_soal->tipe == 'essay') {
				$skor = $input['skor'][$value->id_detail_soal] * $value->detail_soal->weight / 100;
				$totalskor+=$skor;
			}
		}
		$totalskor+=$input['skor_pg'];
		$get->update([
			'nilai' => $totalskor
		]);
		return redirect('/assess_exam/' . $data->id_soal);
	}

	public function statistic()
	{
		return view('guru.statistic');
	}

	public function statistic_detail($id='')
	{
		return view('guru.statistic_detail', [
			'id' => $id
		]);
	}
}
