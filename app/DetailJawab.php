<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailJawab extends Model
{
    protected $table = 'detail_jawab';
    protected $primaryKey = 'id_detail_jawab';
    protected $fillable = [
    	'answer',
    	'id_detail_soal',
    	'id_jawab',
    ];

    public $timestamps = false;

    public function jawab()
    {
    	return $this->hasOne('App\Jawab', 'id_jawab', 'id_jawab');
    }

    public function detail_soal()
    {
    	return $this->hasOne('App\DetailSoal', 'id_detail_soal', 'id_detail_soal');
    }
}
