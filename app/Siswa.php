<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Siswa extends Model
{
    protected $table = 'siswa';
    protected $primaryKey = 'nis';
    protected $fillable = [
        'nis', 'nama_siswa', 'id_kelas',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function user()
    {
        return $this->hasOne('App\User', 'id_user', 'id_user');
    }
}
